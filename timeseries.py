import numpy as np
import pandas as pd
from datetime import datetime, timedelta as delta
import json
import re
from matplotlib import pyplot
import sys
import getopt
import time

# Parameter
# timeseries.py path/to/output/directoy/ inputfile path/to/output/dir/ outputfile

len = len(sys.argv)
print(len)

# Read inputfile as first argument of script
inputpath = sys.argv[1]
inputfile = sys.argv[2]
outputpath = sys.argv[3]
if len > 4:
    outputfile = sys.argv[4]
elif len == 4:
    outputfile = inputfile[:-3] + ".jpg"



#print(outputfile)

#input = inputpath+inputfile

#print('Inputfile is: ', input)

# set dictionarys for timestamp and downloadspeeds
timestamps = []
downloads = []
downloadsMb = []
latency = []
# prepare regex for timestamp extraction
re_timestamp = re.compile('\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}')
re_year = re.compile('\d{4}')
re_month = re.compile('\d{2}')
re_day = re.compile('\d{2}')
re_hour = re.compile('\d{2}')
re_minute = re.compile('\d{2}')
re_second = re.compile('\d{2}')

# open file given as script argument
f = open(input, "r")




# read all lines of the file
Lines = f.readlines()

# go through each line and add valus to the corresponding arrays
for line in Lines:
    theLine = json.loads(line)               # read one line of JSON

    theTimestamp = re_timestamp.match(theLine["timestamp"]).group()
    theYear = int(theTimestamp[:4])
    theMonth = int(theTimestamp[5:7])
    theDay = int(theTimestamp[8:10])
    theHour = int(theTimestamp[11:13])
    theMinute = int(theTimestamp[14:16])
    theSecond = int(theTimestamp[17:19])
    timestamp = datetime(theYear, theMonth, theDay, theHour, theMinute, theSecond)
    timestamps.append(timestamp) # append dictionary-entry for 'timestamp' of current line
    downloads.append(theLine["download"])   # append dictionary-entry for 'download' of current line
    downloadsMb.append(theLine["download"] / (1024*1024))
    latency.append(theLine["ping"])

f.close()
# make timeseries from arrays
tsDown = pd.Series(downloadsMb, index = timestamps)
tsLat = pd.Series(latency, index = timestamps)

# calculate averages of the last 1, 5, 10 and 30 Minutes
downAvg1 = (tsDown.last('1Min').sum()) / 1
downAvg5 = (tsDown.last('5Min').sum()) / 5
downAvg10 = (tsDown.last('10Min').sum()) / 10
downAvg30 = (tsDown.last('30Min').sum()) / 30

latAvg1 = (tsLat.last('1Min').sum()) / 1
latAvg5 = (tsLat.last('5Min').sum()) / 5
latAvg10 = (tsLat.last('10Min').sum()) / 10
latAvg30 = (tsLat.last('30Min').sum()) / 30

downAvgs = [downAvg1, downAvg5, downAvg10, downAvg30]

# save averages in files
names =['/var/www/html/downAvg1', '/var/www/html/downAvg5', '/var/www/html/downAvg10', '/var/www/html/downAvg30']
for name, down  in zip(names, downAvgs) :
    downFile = open(name + ".txt", "w")
    downFile.write(str(down))
    downFile.close()

latAvgs = [latAvg1, latAvg5, latAvg10, latAvg30]

# save averages in files
names =['/var/www/html/latAvg1', '/var/www/html/latAvg5', '/var/www/html/latAvg10', '/var/www/html/latAvg30']
for name, lat  in zip(names, latAvgs) :
    latFile = open(name + ".txt", "w")
    latFile.write(str(lat))
    latFile.close()


# print("downAvg1:" , downAvg1);
# print("downAvg5:" , downAvg5);
# print("downAvg10:" , downAvg10);
# print("downAvg30:" , downAvg30);
#
# print("latAvg1:" , latAvg1);
# print("latAvg5:" , latAvg5);
# print("latAvg10:" , latAvg10);
# print("latAvg30:" , latAvg30);

# plot timeseries
figure, ax1 = pyplot.subplots(figsize=(pyplot.figaspect(0.35)))
color = 'tab:blue'
ax1.set_xlabel('Tageszeit')
ax1.set_ylabel('Dowload in MBit/s', color=color)
ax1.plot(tsDown, color=color);
pyplot.grid()

color = 'tab:red'
ax2 = ax1.twinx()
ax2.set_ylabel('Latenz in ms', color=color)
ax2.plot(tsLat, color=color)

figure.tight_layout()


pyplot.show()



output = outputpath + outputfile

# save plot to .pdf-file
figure.savefig(output, bbox_inches='tight')

# What does it do?
This project logs connection quality indicators (download speed and latency) via speedtest-cli and shows the resulting graphs on a website.

The page is not updateed asynchronosly, so you need to refresh it each time you want the latest values.

# Dependencies
- speedtest-cli: https://wiki.ubuntuusers.de/speedtest-cli/
- Python3:
  - numpy
  - pandas
  - datetime
  - json
  - re
  - matplotlib
  - sys
  - getopt
  - time
- Webserver (e.g. Apache2)
- PHP


# Setup
## Webserver & Scripts
- `cp -r html/ /var/www/html/`
- `mkdir /var/www/html/archive/`
- `mkdir /var/www/html/media/`
- `cp timeseries.py /opt/script/`

## Crontab
Install cronjobs as in `crontab`

## Logrotate
The logfiles used to create the graphs are rotated daily.
It's vital to use the `dateext` and `dateyesterday` options in `/etc/logrotate.conf` as the filenames indicate the date of creation for the graph-jpgs (see `crontab` and `index.php`).

```
# /etc/logrotated.d/speedtest
/var/log/speedtest/speedtest.log {
        daily
        rotate 60
        compress
        delaycompress
        missingok
        notifempty
        create 644 root $USER
}
```
```
# /etc/logrotate.conf
#[...]
# use date as a suffix for the rotated file
dateext
dateyesterday
#[...]
```

# TODO / Missing features
- Accounting for daylight-savings time. Logged/printed timestamps are off
- Asynchronos reload of average-values and current graph-jpg

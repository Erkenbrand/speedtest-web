<!DOCTYPE html>
<html>
<head
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,300italic,700,700italic">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/milligram/1.4.1/milligram.min.css" integrity="sha512-xiunq9hpKsIcz42zt0o2vCo34xV0j6Ny8hgEylN3XBglZDtTZ2nwnqF/Z/TTCc18sGdvCjbFInNd++6q3J0N6g==" crossorigin="anonymous" />
</head>
<body>

<h1>Netzwerkverbindung extern</h1>

<h2>Aktuelle Durchschnittswerte</h2>

<?php
  # Trim averages for download and latency
  $downAvg1 = substr(file_get_contents('downAvg1.txt'), 0, 6) ;
  $downAvg5 = substr(file_get_contents('downAvg5.txt'), 0, 6) ;
  $downAvg10 = substr(file_get_contents('downAvg10.txt'), 0, 6) ;
  $downAvg30 = substr(file_get_contents('downAvg30.txt'), 0, 6) ;

  $latAvg1 = substr(file_get_contents('latAvg1.txt'), 0, 6) ;
  $latAvg5 = substr(file_get_contents('latAvg5.txt'), 0, 6) ;
  $latAvg10 = substr(file_get_contents('latAvg10.txt'), 0, 6) ;
  $latAvg30 = substr(file_get_contents('latAvg30.txt'), 0, 6) ;

  # Print values
  echo "<table>
        <tr><td>1 Min</td><td>" .$downAvg1 ." MBit/s</td><td>" .$latAvg1. " ms</td></tr>";
  echo "<tr><td>5 Min</td><td>" .$downAvg5 ." MBit/s</td><td>" .$latAvg5. " ms</td></tr>";
  echo "<tr><td>10 Min</td><td>" .$downAvg10 ." MBit/s</td><td>" .$latAvg10. " ms</td></tr>";
  echo "<tr><td>30 Min</td><td>" .$downAvg30 ." MBit/s</td><td>" .$latAvg30. " ms</td></tr>";
  echo "</table>";

?>

<?php
  # Read all matching files in array and revert the order
  $files = array_reverse(glob('media/*.log*.{jpg}', GLOB_BRACE));

  # Show the graph for the current day
  echo "<h2>Heute (00:00 bis jetzt)</h2>";
  echo "<div id='img'>";
    echo "<img src='media/current.jpg' width='auto' height='auto'>";
  echo "</div>";

  # For all files in previously loaded array, get day, month and year from filenames.
  # Print graphs and title each one accordingly
  foreach ($files as $file) {
    $year = $month = $day = 0;
    if (preg_match('([0-9])', $file) === 1){
      $date = str_replace("media/speedtest.log-", "", $file);
      $date = str_replace(".jpg", "", $date);

      $year = substr($date, 0, 4);
      $month = substr($date, 4, 2);
      $day = substr($date, 6, 2);
      $cnt++;
    }
    echo "<h2>" .$day. "." .$month. "." .$year ."</h2>";
    echo "<br>";
    echo "<div id='img'>";
      echo "<img src='" . $file ."' width='auto' height='auto'>";
    echo "</div>";
  }

?>


</body>
</html>
